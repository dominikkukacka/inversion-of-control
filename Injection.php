<?php


class Injection {

    public function __construct( Pimple $pimple ) {
        $this->database = $pimple["database"];
        $this->user = $pimple["user"];
    }

    public function getInjections() {
        
        $statement = $this->database->prepare("
            SELECT 
                i.`id`, 
                it.`id` as injection_type_id,
                it.`name` as injection_type_name,
                u.`id` as user_id,
                u.`email` as user_email
            FROM injection i
            LEFT OUTER JOIN `injection_type` it
                ON (
                    i.`_type` = it.`id`
                )
            LEFT OUTER JOIN `user` u
                ON (
                    i.`_user` = u.`id`
                )
            WHERE i.`_user` = ?;
        ");
        $statement->execute(array($this->user->id));
        $data = $statement->fetchAll(PDO::FETCH_CLASS);

        $data = $this->database->groupObject($data, "injection_type_", "injection");
        $data = $this->database->groupObject($data, "user_", "user");

        return $data;

    }

}
