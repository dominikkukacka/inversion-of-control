<?php


class ExtendedPDO extends PDO {

    public function groupObject($obj, $starts_with, $new_key = null) {
        for($i = 0, $cnt = sizeof($obj); $i < $cnt; $i++) {
            $obj[$i] = $this->_groupObject($obj[$i], $starts_with, $new_key);
        }

        return $obj;
    }
    protected function _groupObject( $obj, $starts_with, $new_key = null ) {
        $tmp = array();
        foreach($obj as $k => $v) {
            if( strncmp( $k, $starts_with, strlen($starts_with) ) === 0 ) {

                $key = substr( $k, strlen($starts_with) );
                $tmp[$key] = $v;
                unset($obj->$k);

            }
        }

        $new_key = $new_key !== null ? $new_key : $starts_with; 
        $obj->$new_key = $tmp;
        return $obj;
    }

}
