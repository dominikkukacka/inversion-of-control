<?php

include("Pimple.php");
include("ExtendedPDO.php");
include("User.php");
include("Injection.php");

function pr($p){echo "<pre>".print_r($p,true)."</pre>";}

$container = new Pimple();

$container['database'] = function ($c) {
    return new ExtendedPDO('mysql:host=127.0.0.1;dbname=test', 'user', 'password', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')); ;
};

$container['user'] = function ($c) {
    return new User($c);
};


$injection = new Injection($container);

pr($injection->getInjections());

echo "foobaa";
