<?php

class User {

    protected $_data = array();

    public function __construct( $container ) {
        $this->database = $container["database"];

        $this->_data["id"] = 1;
    } 

    public function __get($name) {
        return $this->_data[$name];
    }

}
